<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="container col-md-6 mt-4">
        <div class="fw-bold bg-primary  text-center text-white p-3 mb-2 rounded">Huruf Vokal</div>

        <div class="form-group">
            <label for="exampleInputEmail1">Kata-Kata Anda</label>
            <input type="text" class="form-control" id="kata" name="kata">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Hasil Anda</label>
            <input type="text" class="form-control" id="hasil" readonly>
        </div>
        <button type="submit" class="btn btn-primary cek">Submit</button>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).on('click', '.cek', function() {
        let kata = $('#kata').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `/hasilvokal`,
            method: "post",
            data: {
                kata: kata,
            },
            dataType: 'json',
            success: function(data) {
                $('#hasil').val(data[0]);
                console.log(data[0]);

            },

        });
    });
</script>

</html>