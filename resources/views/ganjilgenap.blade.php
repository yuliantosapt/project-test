<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container p-5 col-md-4">
        <div class="fw-bold bg-primary  text-center text-white p-3 mb-2 rounded">Ganjil Genap</div>
        <form action="/hasilganjil_genap" method="post">
        @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Angka pertama</label>
                <input type="text" class="form-control" id="pertama" name="pertama">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Angka kedua</label>
                <input type="text" class="form-control" id="kedua" name="kedua">
            </div>

            <div class="text-center">
                <button type="submit" class="btn btn-primary ">Hasil</button>
            </div>
        </form>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).on('click', '.kali', function() {
        let pertama = $('#pertama').val();
        let kedua = $('#kedua').val();
        let kali = $("#kali").attr("name");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `/kalkulator`,
            method: "post",
            data: {
                kali: kali,
                pertama: pertama,
                kedua: kedua,
            },
            dataType: 'json',
            success: function(data) {
                $('#hasil').val(data);
                console.log(data);

            },

        });
    });
    $(document).on('click', '.bagi', function() {
        let pertama = $('#pertama').val();
        let kedua = $('#kedua').val();
        let bagi = $("#bagi").attr("name");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `/kalkulator`,
            method: "post",
            data: {
                bagi: bagi,
                pertama: pertama,
                kedua: kedua,
            },
            dataType: 'json',
            success: function(data) {
                $('#hasil').val(data);
                console.log(data);

            },

        });
    });
    $(document).on('click', '.tambah', function() {
        let pertama = $('#pertama').val();
        let kedua = $('#kedua').val();
        let tambah = $("#tambah").attr("name");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `/kalkulator`,
            method: "post",
            data: {
                tambah: tambah,
                pertama: pertama,
                kedua: kedua,
            },
            dataType: 'json',
            success: function(data) {
                $('#hasil').val(data);
                console.log(data);

            },

        });
    });
    $(document).on('click', '.kurang', function() {
        let pertama = $('#pertama').val();
        let kedua = $('#kedua').val();
        let kurang = $("#kurang").attr("name");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: `/kalkulator`,
            method: "post",
            data: {
                kurang: kurang,
                pertama: pertama,
                kedua: kedua,
            },
            dataType: 'json',
            success: function(data) {
                $('#hasil').val(data);
                console.log(data);

            },

        });
    });
</script>

</html>