<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>

<body>
    <div class="container p-5 col-md-4">
        <div class="fw-bold bg-primary  text-center text-white p-3 mb-2 rounded">Hasil Vokal</div>

        <table class="table table-striped text-center">
            <thead class="">
                <tr>
                    <th scope="col">Hasil</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1; ?>
               @foreach($data as $datas)
                <tr>
                   
                    <th scope="row">{{$datas}}</th>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>