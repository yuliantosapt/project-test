<?php

use App\Http\Controllers\GanjilGenapController;
use App\Http\Controllers\KalkulatorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VokalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});
Route::post('/kalkulator', [KalkulatorController::class, 'result']);
Route::get('/ganjil_genap', [GanjilGenapController::class, 'index']);
Route::post('/hasilganjil_genap', [GanjilGenapController::class, 'result']);
Route::get('/vokal', [VokalController::class, 'index']);
Route::post('/hasilvokal', [VokalController::class, 'result']);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__ . '/auth.php';
