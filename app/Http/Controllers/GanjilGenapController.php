<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ganjilgenap;

class GanjilGenapController extends Controller
{
    public function index()
    {
        return view('ganjilgenap');
    }
    public function result(Request $request)
    {
        $a = $request->pertama;
        $b = $request->kedua;
        for ($i = $a; $i <= $b; $i++) {

            if ($i % 2 == 0) {
                
                ganjilgenap::$box[] = "Angka " . $i . " adalah genap";
                
            } else {

                ganjilgenap::$box[] = "Angka " . $i . " adalah ganjil";
            }
           
            
        }
       
        
        return view('hasilganjilgenap', ['data' =>  ganjilgenap::$box]);
    }
}
