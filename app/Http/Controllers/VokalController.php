<?php

namespace App\Http\Controllers;

use App\Models\vokal;
use Illuminate\Http\Request;

class VokalController extends Controller
{

    public function index()
    {
        return view('vokal');
    }
    public function result(Request $request)
    {

        $a = "aiueo";
        $a_str = str_split($a);
        $kata_str = str_split($request->kata);
        // $save = [];
        for ($i = 0; $i < count($a_str); $i++) {
            for ($j = 0; $j < count($kata_str); $j++) {
                if ($a_str[$i] == $kata_str[$j]) {
                    vokal::$box[] = $a_str[$i];
                }
            }
        }
        $val = null;
        $count = count(array_unique(vokal::$box));
        $arr = array_values(array_unique(vokal::$box));

        for ($i = 0; $i < count($arr); $i++) {
            if (count($arr) == 1) {
                return response([$request->kata . ' = ' . $count . ' yaitu ' . $arr[$i]]);
            } elseif (count($arr) == 2) {

                return response([$request->kata . ' = ' . $count . ' yaitu ' . $arr[$i] . ' dan ' . $arr[$i + 1]]);
            } elseif (count($arr) == 3) {

                return response([$request->kata . ' = ' . $count . ' yaitu ' . $arr[$i] . ' dan ' . $arr[$i + 1] . ' dan ' . $arr[$i + 2]]);
            } elseif (count($arr) == 4) {

                return response([$request->kata . ' = ' . $count . ' yaitu ' . $arr[$i] . ' dan ' . $arr[$i + 1] . ' dan ' . $arr[$i + 2] . ' dan ' . $arr[$i + 3]]);
            } elseif (count($arr) == 5) {

                return response([$request->kata . ' = ' . $count . ' yaitu ' . $arr[$i] . ' dan ' . $arr[$i + 1] . ' dan ' . $arr[$i + 2] . ' dan ' . $arr[$i + 3] . ' dan ' . $arr[$i + 4]]);
            }
        }
    }
}
