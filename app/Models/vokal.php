<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vokal extends Model
{
    use HasFactory;

    public static $box = [];
    public static $result = null;

    public function __construct($result)
    {
     $this->result = $result;   
    }
}
